import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'poc-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent {

  @Input() items: string[];

}
