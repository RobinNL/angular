import { Component, Input, OnInit } from '@angular/core';

export type ButtonType = 'primary' | 'secondary';

@Component({
  selector: 'poc-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent {

  @Input() type: ButtonType;
  @Input() block: boolean;

  handleClick() {
    alert('my internal click');
  }

}
