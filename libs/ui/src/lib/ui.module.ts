import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ButtonComponent } from './button/button.component';
import { ImageComponent } from './image/image.component';
import { ListComponent } from './list/list.component';
import { MousePositionComponent } from './mouse-position/mouse-position.component';

@NgModule({
  imports: [
      CommonModule
  ],
  declarations: [
      ButtonComponent,
      ImageComponent,
      ListComponent,
      MousePositionComponent
  ],
  exports: [
      ButtonComponent,
      ImageComponent,
      ListComponent,
      MousePositionComponent
  ]
})
export class UiModule {}
