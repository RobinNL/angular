import { Component, ViewChild } from '@angular/core';

@Component({
    selector: 'poc-mouse-position',
    templateUrl: './mouse-position.component.html',
    styleUrls: ['./mouse-position.component.scss']
})
export class MousePositionComponent {

    @ViewChild('mouseDemo') mouseDemo;

    xPos = 0;
    yPos = 0;

    backgroundColor: string;

    handleMousePosition(e: MouseEvent) {
        this.xPos = e.clientX;
        this.yPos = e.clientY;
        this.calculatePercent();
    }

    calculatePercent() {
        const relativePosY = (this.yPos - (this.mouseDemo.nativeElement.offsetTop - window.scrollY)) / this.mouseDemo.nativeElement.clientHeight;
        const relativePosX = (this.xPos - this.mouseDemo.nativeElement.offsetLeft) / this.mouseDemo.nativeElement.clientWidth;
        this.backgroundColor = `hsl(${Math.floor(360 * relativePosX)} ${Math.floor(100 * relativePosY)}% 50%)`;
    }


}
