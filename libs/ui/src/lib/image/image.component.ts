import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'poc-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent {

  @Input() source: string;
  active = false;

}
