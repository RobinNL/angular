import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavigationComponent } from './shared/navigation/navigation.component';
import { UiModule } from '@poc/ui';
import { RouterModule } from '@angular/router';
import { BasicComponent } from './basic/basic.component';
import { FormComponent } from './form/form.component';
import { AnimationComponent } from './animation/animation.component';
import { DataComponent } from './data/data.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
    declarations: [
        AppComponent,
        NavigationComponent,
        BasicComponent,
        FormComponent,
        AnimationComponent,
        DataComponent
    ],
    imports: [
        BrowserModule,
        UiModule,
        BrowserAnimationsModule,
        RouterModule.forRoot([
            {
                path: 'form',
                component: FormComponent
            },
            {
                path: 'basic',
                component: BasicComponent
            },
            {
                path: 'animation',
                component: AnimationComponent
            },
            {
                path: 'data',
                component: DataComponent
            }
        ])],
    bootstrap: [
        AppComponent
    ],
})
export class AppModule {
}
