import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'poc-basic',
  templateUrl: './basic.component.html',
  styleUrls: ['./basic.component.scss']
})
export class BasicComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
