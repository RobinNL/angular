import { Component } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
    selector: 'poc-animation',
    templateUrl: './animation.component.html',
    animations: [
        trigger('appear', [
          transition(':enter', [
            style({ opacity: 0 }),
            animate('100ms', style({ opacity: 1 })),
          ]),
          transition(':leave', [
            animate('100ms', style({ opacity: 0 }))
          ])
        ])
    ],
    styleUrls: ['./animation.component.scss']
})
export class AnimationComponent {

    active = false;
}
